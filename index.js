// const functions = require("firebase-functions");

var express = require('express');
const cors = require('cors');
var indexRouter = require('./routes/index');
var movieRouter = require('./routes/movie');

var app = express();
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use('/', indexRouter);
// app.use('/movie', movieRouter);

app.listen(process.env.PORT || 443, () => console.log(`Run Port ${process.env.PORT || 443}`));