var express = require('express');
var router = express.Router();
const request = require('request');
const HTMLParser = require('node-html-parser');
var admin = require("firebase-admin");
var serviceAccount = require(__dirname + "/serviceAccountKey.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://anime-api-af788.firebaseio.com"
});

const db = admin.database()

router.get('/hls/', (req, res) => {
  // res.set('Cache-Control', 'public, max-age=31557600');
  const domain = `https://api.lv51anime.com`
  var url = req.query.file

  var options = {
    "rejectUnauthorized": false,
    'method': 'GET',
    'gzip': true,
    'encoding': 'utf-8',
    'url': `${url}`,
    'headers': {
      'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36',
      'Referer': 'https://www.anime-master.com/',
      'Origin': 'https://www.anime-master.com/'
    }
  };
  if (url.indexOf('.ts') != -1) {
    res.setHeader("content-disposition", "attachment; filename=video.png");
    request(url).pipe(res)
  } else {
    request(options, function (error, response, body) {
      if (url.indexOf('moekawaii.stream') != -1) {
        body = body.toString()
        if (url.indexOf('/index') != -1) {
          var urlbase = url.split('/index')
          body = body.split('https://').join(`${domain}/hls?base=${urlbase[0]}&file=https://`)
        } else {
          body = body.split('https://').join(`${domain}/hls?file=https://`)
        }
      }

      if (body.indexOf('seg-') != -1 && body.indexOf('EXT-X-MEDIA-SEQUENCE') != -1) {
        body = body.toString()
        if (req.query.base) {
          var urlbase = req.query.base
        } else {
          var urlbase = url.split('/index')
        }
        body = body.split('seg-').join(`${domain}/hls?file=${urlbase[0]}/seg-`)
      }
      res.end(body);
    })
  }
})

function getVideoAnimeMaster(url) {
  var options = {
    "rejectUnauthorized": false,
    'method': 'GET',
    'gzip': true,
    'encoding': null,
    'url': `${url}`,
  };
  return new Promise(rest => {
    request(options, function (error, response, body) {
      if (body) {
        const regex = /https?:\/\/.?\.?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b[-a-zA-Z0-9@:%_\+.~#?&\/\/=,]*.m3u8/g;
        m = regex.exec(body)
        let urlplay = false
        m.forEach((match, groupIndex) => {
          urlplay = `${match}`;
        });

        rest(urlplay)
      } else {
        rest(false)
      }
    })
  })
}

function getVideo(url) {
  var options = {
    "rejectUnauthorized": false,
    'method': 'GET',
    'gzip': true,
    'encoding': null,
    'url': `${url}`,
  };
  return new Promise(rest => {
    request(options, async function (error, response, body) {
      const root = HTMLParser.parse(body)
      var q = root.querySelector('iframe')
      if (q) {
        var url = q.getAttribute('src');
        if (url.indexOf('anime-master.com') == -1 && url.indexOf('No m3u8Id') == -1) {
          if (url.indexOf('hls.balance-xd.co') != -1) {
            rest(`${url.replace('https://hls.balance-xd.co/public/dist/index.html?id=', 'https://hls.balance-xd.co/playlist/')}/play.m3u8`)
          } else {
            rest(url);
          }
        } else {
          if (url.indexOf('anime-master.com/direct.php') != -1) {
            if (url.indexOf('http') == -1) {
              rest(await getVideoAnimeMaster(`https:${url}`))
            } else {
              rest(await getVideoAnimeMaster(`${url}`))
            }
          } else {
            rest(false)
          }
        }

      } else {
        rest(false)
      }

    });
  })
}

function htmlPlayer(urlplayer, status) {
  var arra = [`<a href="http://bit.ly/vpsfree100us">
  <div style="
text-align: center;
color: red;
font-weight: bold;
">สมัคร VPS เต็มเงิน 10us รับเพิ่ม 100us</div>
  <img src="https://bit.ly/39qt25O" width="468" height="60">
</a>`, `<a href="https://bit.ly/viplottovip" target="_blank">
  <img src="https://bit.ly/3bzQxvR" style="
    height: 50px;
    opacity: 0.8;
  ">
    </a>`]
  var html = `<div style="
  z-index: 9999999999;
  position: absolute;
  top: 5%;
  right: 5%;
">
  ${arra[Math.floor(Math.random() * arra.length)]}
</div>`


  if (urlplayer.indexOf('ok.ru') != -1) {
    return `<style>
  body {
    height: 100%;
    width: 100%;
    margin: 0;
    padding: 0;
}
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<iframe style="height: 100%;width: 100%;border: 0px;" src="${urlplayer}" allowfullscreen="allowfullscreen"></iframe>
<script type="text/JavaScript">
  setTimeout(()=>{
    document.getElementsByClassName('jw-media')[0]
    $('#media-player .jw-media.jw-reset').append(\`${html}\`)
    },5000)

        </script>`

  } else {

    return `<style>
  body {
    height: 100%;
    width: 100%;
    margin: 0;
    padding: 0;
}
</style>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://hls.balance-xd.co/vanlongstream/player/v/8.8.2/jwplayer.js?v=9"></script>
<script type="text/javascript">jwplayer.key = "ITWMv7t88JGzI0xPwW8I0+LveiXX9SWbfdmt0ArUSyc=";</script>
${status}
<div id="media-player"></div>
<script type="text/JavaScript">
         var playerInstance = jwplayer("media-player");
         playerInstance.setup({
       sources:[{file: "${urlplayer}", label: "hls P", "type": "hls" },],
           width: "100%",
           height: "100%",
           cast: {},
           playbackRateControls: "true"
          });
if(playerInstance.on('ready')){
  setTimeout(()=>{
    document.getElementsByClassName('jw-media')[0]
    $('#media-player .jw-media.jw-reset').append(\`${html}\`)
    },5000)
}
        </script>`
  }
}
router.get('/play/:id/:link', async function (req, res, next) {
  // res.set('Cache-Control', 'public, max-age=31557600');
  res.set('content-type', 'text/html; charset=utf-8')
  const domain = `https://api.lv51anime.com`
  var urlplayer = await getVideo(`https://www.anime-master.com/embed/${req.params.id}&multi=link${req.params.link}`)
  if (urlplayer) {
    if (urlplayer.indexOf('moekawaii.stream') != -1) {
      res.end(htmlPlayer(`${domain}/hls/?file=${urlplayer}`, '<!-- buy at www.cii3.net -->'))
    } else {
      res.end(htmlPlayer(`${urlplayer}`, '<!-- buy at www.cii3.net -->'))
    }
  } else {
    res.end(htmlPlayer(
      'https://content.jwplatform.com/manifests/yp34SRmf.m3u8',
      '<p style="position: absolute;background: red;z-index: 9999;height: 70px;width: 400px;font-size: xx-large;text-align: center;line-height: 70px;color: white;">Error Waiting for correction</p>'
    ))
  }
})

router.get('/ep/:slug', function (req, res, next) {
  // res.set('Cache-Control', 'public, max-age=31557600');
  const domain = `https://api.lv51anime.com`
  var url = `https://www.anime-master.com/${encodeURIComponent(req.params.slug)}`
  var options = {
    "rejectUnauthorized": false,
    'method': 'GET',
    'gzip': true,
    'encoding': null,
    'url': `${url}`,
  };
  const regex = /https:\/\/www.anime-master\.com\/embed\/\d*/g;
  request(options, async function (error, response, body) {
    const root = HTMLParser.parse(body)

    let m;
    let playlink = ''

    m = regex.exec(body)

    m.forEach((match, groupIndex) => {
      playlink = `${match}`;
    });
    var link = playlink.replace('https://www.anime-master.com/embed/', '').replace('https://anime-master.com/embed/', '')

    const array_new = []
    root.querySelector('#bs-example-navbar-collapse-1').querySelectorAll('li').forEach(element => {
      if (element.getAttribute('onclick')) {
        array_new.push(`${domain}/play/${link}/${element.getAttribute('onclick').replace('load_video_http(', '').replace(')', '').replace('if (!window.__cfRLUnblockHandlers return false; ', '').replace(')', '')}`)
      }
    });

    res.json(array_new);
  });
})

router.get('/searchanime/:key', async function (req, res, next) {
  // res.set('Cache-Control', 'public, max-age=31557600');
  const domain = `https://api.lv51anime.com`
  var url = `https://www.anime-master.com/search/function.php?getData=${encodeURIComponent(req.params.key)}`
  var options = {
    "rejectUnauthorized": false,
    'method': 'GET',
    'gzip': true,
    'encoding': null,
    'url': `${url}`,
  };
  const array = []
  request(options, function (error, response, body) {
    const root = HTMLParser.parse(body);

    root.querySelectorAll('.list-group-item').forEach((element, index) => {
      console.log(element.toString())
      array.push({
        link: element.getAttribute('href').replace('https://www.anime-master.com/', domain + '/detail/').replace('https://anime-master.com/', domain + '/detail/'),
        title: element.getAttribute('onselect').split("('")[1].split("')")[0],
        slug: element.getAttribute('href').replace('https://www.anime-master.com/', '').replace('/', '').replace('https://anime-master.com/', '').replace('/', ''),
        image: element.querySelector('img').getAttribute('src').replace('https://www.anime-master.com/', 'https://api.lv51anime.com/image/').replace('https://anime-master.com/', 'https://api.lv51anime.com/image/'),
      })
    });

    res.json(array);
  });
})
router.get('/image/:type/:name', async function (req, res, next) {
  res.set('Cache-Control', 'public, max-age=31557600');
  var type = req.params.name.split('.')
  res.type(type[type.length - 1])

  var options = {
    "rejectUnauthorized": false,
    'method': 'GET',
    'gzip': true,
    'encoding': null,
    'url': `https://www.anime-master.com/${req.params.type}/${encodeURIComponent(req.params.name)}`,
  };
  request.get(options, (e, r, h) => {
    res.end(h)
  })
})

router.get('/topanime', async function (req, res, next) {
  // res.set('Cache-Control', 'public, max-age=31557600');
  var showJson = await new Promise(rest => {
    db.ref(`animetop`).once('value', snap => {
      var data = snap.val()
      if (!data) {
        rest(true)
      } else {
        res.json(data);
        rest(false)
      }
    })
  })

  const domain = `https://api.lv51anime.com`
  var url = `https://www.anime-master.com/`
  var options = {
    "rejectUnauthorized": false,
    'method': 'GET',
    'gzip': true,
    'encoding': null,
    'url': `${url}`,
  };
  const array = []
  request(options, function (error, response, body) {
    const root = HTMLParser.parse(body);

    root.querySelectorAll('.panel.panel-primary .media').forEach((element, index) => {
      var v = element.querySelector('a')
      var slug = v.getAttribute('href').replace('https://www.anime-master.com/', '').replace('/', '').replace('https://anime-master.com/', '').replace('/', '')
      array.push({
        link: v.getAttribute('href').replace('https://www.anime-master.com/', domain + '/detail/').replace('https://anime-master.com/', domain + '/detail/'),
        title: v.getAttribute('title'),
        slug: slug,
        image: v.querySelector('.popular_lnwphp').getAttribute('data-src').replace('https://www.anime-master.com/', 'https://api.lv51anime.com/image/').replace('https://anime-master.com/', 'https://api.lv51anime.com/image/'),
      })
    });
    db.ref(`animetop`).set(array)
    if (showJson) {


      res.json(array);
    }
  });
})

router.get('/category', async function (req, res, next) {
  // res.set('Cache-Control', 'public, max-age=31557600');
  var showJson = await new Promise(rest => {
    db.ref(`animecategory`).once('value', snap => {
      var data = snap.val()
      if (!data) {
        rest(true)
      } else {
        res.json(data);
        rest(false)
      }
    })
  })

  const domain = `https://api.lv51anime.com`
  var url = `https://www.anime-master.com/`
  var options = {
    "rejectUnauthorized": false,
    'method': 'GET',
    'gzip': true,
    'encoding': null,
    'url': `${url}`,
  };
  const array = []
  request(options, function (error, response, body) {
    const root = HTMLParser.parse(body);

    root.querySelectorAll('.nav.nav-pills.nav-stacked li').forEach((element, index) => {
      if (index != 0) {
        array.push({
          link: element.querySelector('a').getAttribute('href').replace('https://www.anime-master.com/category/', `${domain}/list/?category=`).replace('https://anime-master.com/category/', `${domain}/list/?category=`).split('/').slice(0, -1).join('/'),
          title: element.innerText.trim(),
          slug: element.querySelector('a').getAttribute('href').replace('https://www.anime-master.com/category/', '').replace('https://anime-master.com/category/', '').split('/').slice(0, -1).join('/'),
          linktotal: element.querySelector('a').getAttribute('href').replace('https://www.anime-master.com/category/', `${domain}/list/total/?category=`).replace('https://anime-master.com/category/', `${domain}/list/total/?category=`).split('/').slice(0, -1).join('/')
        })
      }
    });
    db.ref(`animecategory`).set(array)
    if (showJson) {


      res.json(array);
    }
  });
})

router.get('/detail/:slug', async function (req, res, next) {
  // res.set('Cache-Control', 'public, max-age=31557600');
  const domain = `https://api.lv51anime.com`
  const slug = req.params.slug
  if (slug) {

    var showJson = await new Promise(rest => {
      db.ref(`animeep/${slug}`).once('value', snap => {
        var data = snap.val()
        if (!data) {
          rest(true)
        } else {
          res.json(data);
          rest(false)
        }
      })
    })

    var url = `https://www.anime-master.com/${encodeURIComponent(req.params.slug)}`
    var options = {
      "rejectUnauthorized": false,
      'method': 'GET',
      'gzip': true,
      'encoding': null,
      'url': `${url}`,
    };

    request(options, function (error, response, body) {
      const root = HTMLParser.parse(body);
      const array = {
        title: root.querySelector('.col-md-6 img').getAttribute('alt'),
        image: root.querySelector('.col-md-6 img').getAttribute('data-cfsrc').replace('https://www.anime-master.com/', 'https://api.lv51anime.com/image/').replace('https://anime-master.com/', 'https://api.lv51anime.com/image/'),
        detail: root.querySelector('.panel.panel-info p').innerText,
        ep: []
      };
      root.querySelectorAll('td a').forEach(element => {
        array.ep.push({
          title: element.innerText,
          slug: element.getAttribute('href').replace('https://www.anime-master.com/', '').replace('/', '').replace('https://anime-master.com/', '').replace('/', ''),
          link: element.getAttribute('href').replace('https://www.anime-master.com/', domain + '/ep/').replace('https://anime-master.com/', domain + '/ep/'),
        })
      });
      db.ref(`animeep/${slug}`).set(array)
      if (showJson) {
        res.json(array);
      }
    });
  } else {
    res.end('error')
  }
})

function getTotal(query, totalpage) {
  if (query.step != 'stop') {
    console.log(query)
    return new Promise(rest => {
      var url = `https://www.anime-master.com/category/${encodeURIComponent('อนิเมะทั้งหมด')}/`
      if (query.category && query.page) {
        url = `https://www.anime-master.com/category/${encodeURIComponent(query.category)}/?number=${query.page}`

      } else if (query.category) {
        url = `https://www.anime-master.com/category/${encodeURIComponent(query.category)}/`
      } else if (query.page) {
        url = `https://www.anime-master.com/category/${encodeURIComponent('อนิเมะทั้งหมด')}/?number=${query.page}`
      }

      var options = {
        "rejectUnauthorized": false,
        'method': 'GET',
        'gzip': true,
        'encoding': null,
        'url': `${url}`,
      };

      request(options, async function (error, response, body) {
        const root = HTMLParser.parse(body);
        var listanime = root.querySelectorAll('.col-xs-6.col-sm-3.col-md-3.center_lnwphp')
        totalpage += listanime.length
        var pageview = parseInt(root.querySelector('.pagination li.active').innerText)
        var next_page = listanime.length == 32 ? pageview + 1 : false

        if (next_page) {
          rest(await getTotal({ ...query, page: next_page, step: 'next' }, totalpage))
        } else {
          rest({ ...query, page: next_page, step: 'stop', totalpage: totalpage })
        }
      });
    })
  }
}

async function getSumTotal(query) {
  return await getTotal(query, 0)
}

router.get(['/list', '/list/:total'], async function (req, res, next) {
  // res.set('Cache-Control', 'public, max-age=31557600');
  if (req.params.total) {

    var category = req.query.category ? req.query.category : 'ทั้งหมด'

    var showJson = await new Promise(rest => {
      db.ref(`total_anime/${category}`).once('value', snap => {
        var data = snap.val()
        if (!data) {
          rest(true)
        } else {
          res.json(data);
          rest(false)
        }
      })
    })

    getSumTotal({ ...req.query, step: 'next' }).then(getdata => {
      db.ref(`total_anime/${category}`).set({ total: getdata.totalpage, category: category })
      if (showJson) {
        res.json({ total: getdata.totalpage, category: category })
      }
    })

  } else {
    const domain = `https://api.lv51anime.com`
    var sys_url = `${domain}/list/?page=`
    var url = `https://www.anime-master.com/category/${encodeURIComponent('อนิเมะทั้งหมด')}/`
    if (req.query.category && req.query.page) {
      url = `https://www.anime-master.com/category/${encodeURIComponent(req.query.category)}/?number=${req.query.page}`
      sys_url = `${domain}/list/?category=${encodeURIComponent(req.query.category)}&page=`
    } else if (req.query.category) {
      url = `https://www.anime-master.com/category/${encodeURIComponent(req.query.category)}/`
      sys_url = `${domain}/list/?category=${encodeURIComponent(req.query.category)}&page=`
    } else if (req.query.page) {
      url = `https://www.anime-master.com/category/${encodeURIComponent('อนิเมะทั้งหมด')}/?number=${req.query.page}`
    }

    var options = {
      "rejectUnauthorized": false,
      'method': 'GET',
      'gzip': true,
      'encoding': null,
      'url': `${url}`,
    };

    request(options, function (error, response, body) {

      const root = HTMLParser.parse(body);
      var listanime = root.querySelectorAll('.col-xs-6.col-sm-3.col-md-3.center_lnwphp')


      var pageview = parseInt(root.querySelector('.pagination li.active').innerText)
      const array = {
        next_page: `${sys_url}${listanime.length == 32 ? pageview + 1 : pageview}`,
        prev_page: `${sys_url}${pageview <= 1 ? pageview : pageview - 1}`,
        title: root.querySelector('.col-lg-9 .panel.panel-primary .panel-heading .panel-title').innerText,
        list: []
      };
      listanime.forEach(element => {
        var slug = element.querySelector('a').getAttribute('href').replace('https://www.anime-master.com/', '').replace('/', '').replace('https://anime-master.com/', '').replace('/', '')
        var anime = {
          title: element.querySelector('.title_movie').innerText,
          thumbnail: element.querySelector('.img_lnwphp').getAttribute('data-cfsrc').replace('https://www.anime-master.com/', 'https://api.lv51anime.com/image/').replace('https://anime-master.com/', 'https://api.lv51anime.com/image/'),
          image: element.querySelector('.img_lnwphp').getAttribute('data-src').replace('https://www.anime-master.com/', 'https://api.lv51anime.com/image/').replace('https://anime-master.com/', 'https://api.lv51anime.com/image/'),
          slug: slug,
          link: element.querySelector('a').getAttribute('href').replace('https://www.anime-master.com/', domain + '/detail/').replace('https://anime-master.com/', domain + '/detail/'),
          category: element.querySelector('.label-cate') ? element.querySelector('.label-cate').innerText : false,
          status: element.querySelector('.label-cate2') ? element.querySelector('.label-cate2').innerText : false
        }
        db.ref(`anime/${slug}`).once('value', snap => {
          var data = snap.val()
          if (!data) {
            db.ref(`anime/${slug}`).set(anime)
          } else {
            if (data.title != anime.title) {
              db.ref(`anime/${slug}`).set(anime)
            }
          }
        })

        array.list.push(anime)
      });
      res.json(array);
    });
  }
});

module.exports = router;
