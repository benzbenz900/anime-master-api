var express = require('express');
var router = express.Router();
const request = require('request');
const HTMLParser = require('node-html-parser');

router.get('/ep/:mid-:ep', async function (req, res, next) {
    var mid = ''
    if (req.params.mid) {
        mid = req.params.mid
    }
    var ep = '0'
    if (req.params.ep) {
        ep = req.params.ep
    }

    var url = `https://www.doomovie-hd.com/?r=ajax`
    let options = {
        'method': 'POST',
        'url': url,
        formData: {
            'do': 'movie_generate_video_source',
            'movie_id': mid,
            'disc': ep,
            'sel_vidlink_index': '0'
        }
    };
    request(options, function (error, response, body) {
        if (body.indexOf('BAD_INPUT') != -1) {
            const domain = `${req.headers['x-forwarded-proto'] ? req.headers['x-forwarded-proto'] : req.protocol}://${req.get('host')}`
            var url = `https://www.doomovie-hd.com/?r=movie_view&id=${mid}`
            var options = {
                "rejectUnauthorized": false,
                'method': 'GET',
                'gzip': true,
                'encoding': null,
                'url': `${url}`,
            };

            request(options, function (error, response, body) {

                const root = HTMLParser.parse(body);
                var str = root.querySelector('article script').innerText.replace(/var /g, '"').replace(/;/g, ',').replace(/ =/g, '":').replace(/\/r\/n/).trim()
                var data = JSON.parse('{' + str.substring(0, str.length - 1) + '}')

                var poster = root.querySelector('.movie_poster img').getAttribute('src')
                var title = root.querySelector('.movie_name-container').innerText.trim()
                var plot = root.querySelector('.movie_plot').innerText.trim()
                var arr = []
                for (let index = 1; index <= data.g_seriesNumDisc; index++) {
                    arr.push({
                        link: `${domain}/movie/data/?mid=${mid}&ep=${index}`,
                        slug: index,
                        title: `ตอนที่ ${index}`
                    })
                }
                res.json({
                    is_ep: true,
                    title: title,
                    detail: plot,
                    image: `${poster}`,
                    ep: arr
                })

            });

        } else {
            var data = JSON.parse(body)

            var hls = data.direct_hls2
            res.json(hls.map(function (e) {
                if (e) {
                    return 'https://lv51movie.com/play/' + e.replace('https://cdn.livedoomovies.com/playlist/', '').replace('https://cdn.youlive-cdn.com/playlist/', '')
                }
            }).filter((e) => { return e }))

        }
    })
});

router.get('/detail/:mid', async function (req, res, next) {
    var mid = ''
    if (req.params.mid) {
        mid = req.params.mid
    }
    var ep = '0'
    if (req.query.ep) {
        ep = req.query.ep
    }
    var full = false
    if (req.query.full) {
        full = req.query.full
    }
    var url = `https://www.doomovie-hd.com/?r=ajax`
    let options = {
        'method': 'POST',
        'url': url,
        formData: {
            'do': 'movie_generate_video_source',
            'movie_id': mid,
            'disc': ep,
            'sel_vidlink_index': '0'
        }
    };
    request(options, function (error, response, body) {
        if (body.indexOf('BAD_INPUT') != -1) {
            const domain = `${req.headers['x-forwarded-proto'] ? req.headers['x-forwarded-proto'] : req.protocol}://${req.get('host')}`
            var url = `https://www.doomovie-hd.com/?r=movie_view&id=${mid}`
            var options = {
                "rejectUnauthorized": false,
                'method': 'GET',
                'gzip': true,
                'encoding': null,
                'url': `${url}`,
            };

            request(options, function (error, response, body) {

                const root = HTMLParser.parse(body);
                var str = root.querySelector('article script').innerText.replace(/var /g, '"').replace(/;/g, ',').replace(/ =/g, '":').replace(/\/r\/n/).trim()
                var data = JSON.parse('{' + str.substring(0, str.length - 1) + '}')

                var poster = root.querySelector('.movie_poster img').getAttribute('src')
                var title = root.querySelector('.movie_name-container').innerText.trim()
                var plot = root.querySelector('.movie_plot').innerText.trim()
                var arr = []
                for (let index = 1; index <= data.g_seriesNumDisc; index++) {
                    arr.push({
                        link: `${domain}/movie/data/?mid=${mid}&ep=${index}`,
                        slug: `${mid}-${index}`,
                        title: `ตอนที่ ${index}`
                    })
                }
                res.json({
                    is_ep: true,
                    title: title,
                    detail: plot,
                    image: `${poster}`,
                    ep: arr
                })

            });

        } else {
            var data = JSON.parse(body)
            var url2 = `https://www.doomovie-hd.com/?r=movie_view&id=${mid}`
            var options2 = {
                "rejectUnauthorized": false,
                'method': 'GET',
                'gzip': true,
                'encoding': null,
                'url': `${url2}`,
            };

            request(options2, function (error, response, body2) {

                const root2 = HTMLParser.parse(body2);

                var poster = root2.querySelector('.movie_poster img').getAttribute('src')
                var title = root2.querySelector('.movie_name-container').innerText.trim()
                var plot = root2.querySelector('.movie_plot').innerText.trim()


                if (full) {
                    res.json({
                        title: title,
                        detail: plot,
                        image: `${poster}`,
                        ep: [],
                        ...data
                    })
                } else {
                    var hls = data.direct_hls2
                    res.json({
                        is_ep: false,
                        title: title,
                        detail: plot,
                        image: `${poster}`,
                        // player: data.param,
                        hls: hls.map(function (e) {
                            if (e) {
                                return e.replace('https://cdn.livedoomovies.com/playlist/', '').replace('https://cdn.youlive-cdn.com/playlist/', '')
                            }
                        }).filter((e) => { return e }),
                        ep: []
                    })
                }

            });

        }
    })
});

router.get('/category', async function (req, res, next) {
    const domain = `${req.headers['x-forwarded-proto'] ? req.headers['x-forwarded-proto'] : req.protocol}://${req.get('host')}`
    var url = `https://www.doomovie-hd.com/`
    var options = {
        "rejectUnauthorized": false,
        'method': 'GET',
        'gzip': true,
        'encoding': null,
        'url': `${url}`,
    };

    request(options, function (error, response, body) {

        const root = HTMLParser.parse(body);
        var arr = []
        root.querySelectorAll('#nav-bar_non-mobile-menu .dropdown-item').forEach(e => {
            arr.push({
                slug: e.getAttribute('href').replace('./?r=movie&cate=', '').trim(),
                // cateid: `${domain}/movie/list?cate=${e.getAttribute('href').replace('./?r=movie&cate=', '').trim()}&page=1`,
                title: e.innerText.replace('🎬', '').trim()
            })
        });

        var type = []
        // root.querySelectorAll('.left-schedule.text-center .list-group').forEach((e, i) => {
        //     if (i == 1) {
        //         e.querySelectorAll('a').forEach(element => {
        //             var typed = element.getAttribute('href').replace('./?r=movie_filter_by_genre&genre=', '').trim()
        //             if (typed != '') {
        //                 type.push({
        //                     slug: typed,
        //                     title: typed,
        //                     // api: `${domain}/movie/list?type=${typed}&page=1`
        //                 })
        //             }
        //         });
        //     }
        // });

        var year = []
        // root.querySelectorAll('.left-schedule.text-center .left-schedule-inner .badge.badge-error.border-light a').forEach((e, i) => {
        //     var yeard = e.getAttribute('href').replace('./?r=movie_filter_by_year&year=', '').trim()
        //     if (yeard != '') {
        //         year.push({
        //             slug: yeard,
        //             title: yeard,
        //             // api: `${domain}/movie/list?year=${yeard}&page=1`
        //         })
        //     }
        // });

        res.json({ cate: arr, type: type, year: year })
    });
});

router.get('/list', async function (req, res, next) {
    const domain = `${req.headers['x-forwarded-proto'] ? req.headers['x-forwarded-proto'] : req.protocol}://${req.get('host')}`

    var cate = '1'
    if (req.query.category) {
        cate = req.query.category
    }
    var page = '1'
    if (req.query.page) {
        page = req.query.page
    }
    var type = ''
    var year = ''
    if (req.query.type) {
        var url = `https://www.doomovie-hd.com/?r=movie_filter_by_genre&genre=${req.query.type}&page=${page}`
        type = req.query.type
    } else if (req.query.year) {
        var url = `https://www.doomovie-hd.com/?r=movie_filter_by_year&year=${req.query.year}&page=${page}`
        year = req.query.year
    } else {
        var url = `https://www.doomovie-hd.com/?r=movie&cate=${cate}&page=${page}`
    }


    var options = {
        "rejectUnauthorized": false,
        'method': 'GET',
        'gzip': true,
        'encoding': null,
        'url': `${url}`,
    };

    request(options, function (error, response, body) {

        const root = HTMLParser.parse(body);
        var title = root.querySelector('.page-section-h').innerText
        var pagination = root.querySelector('.pagination') ? root.querySelector('.pagination').lastChild.querySelector('.page-link').getAttribute('href').replace(`./?r=movie&cate=${cate}&page=`, '').replace(`./?r=movie_filter_by_genre&genre=${type}&page=`, '').replace(`./?r=movie_filter_by_year&year=${year}&page=`, '').trim() : 1
        var arr = []
        root.querySelectorAll('.movie-card2-outer').forEach(e => {
            var d = e.querySelector('.movie-card2_table').innerText.trim().split('&nbsp;&nbsp;')
            var ribbon = e.querySelector('.movie-ribbon-container')
            var mid = e.querySelector('a').getAttribute('href').replace('./?r=movie_view&id=', '').trim()
            arr.push({
                slug: mid,
                title: e.querySelector('.movie-card2_txt').innerText.trim(),
                category: ribbon ? ribbon.innerText.trim() : null,
                // view: e.querySelector('.movie-rating-container').innerText.trim(),
                status: d[1].trim(),
                // star: d[2].trim(),
                // time: d[3].trim() + ':00',
                // image: e.querySelector('.movie-card2_img').getAttribute('style').replace('background:url(', '').replace(');', '').trim(),
                thumbnail: e.querySelector('.movie-card2_img').getAttribute('style').replace('background:url(', '').replace(');', '').trim(),
                // dataurl: `${domain}/movie/data/?mid=${mid}`
            })
        });

        res.json({
            title: title,
            totalpage: parseInt(pagination),
            list: arr
        })
    });
});

router.get('/search', async function (req, res, next) {
    const domain = `${req.headers['x-forwarded-proto'] ? req.headers['x-forwarded-proto'] : req.protocol}://${req.get('host')}`

    var key = ''
    if (req.query.key) {
        key = req.query.key
    }

    var page = '1'
    if (req.query.page) {
        page = req.query.page
    }

    var url = `https://www.doomovie-hd.com/?r=search&name=${encodeURIComponent(key)}&page=${page}`



    var options = {
        "rejectUnauthorized": false,
        'method': 'GET',
        'gzip': true,
        'encoding': null,
        'url': `${url}`,
    };

    request(options, function (error, response, body) {

        const root = HTMLParser.parse(body);
        var title = root.querySelector('.alert.alert-warning').innerText
        var pagination = root.querySelector('.pagination') ? root.querySelector('.pagination').lastChild.querySelector('.page-link').getAttribute('href').replace(`./?r=search&name=${key}&page=`, '').trim() : 1
        var arr = []
        root.querySelectorAll('.movie-card2-outer').forEach(e => {
            var d = e.querySelector('.movie-card2_table').innerText.trim().split('&nbsp;&nbsp;')
            var ribbon = e.querySelector('.movie-ribbon-container')
            var mid = e.querySelector('a').getAttribute('href').replace('./?r=movie_view&id=', '').trim()
            arr.push({
                mid: mid,
                name: e.querySelector('.movie-card2_txt').innerText.trim(),
                ribbon: ribbon ? ribbon.innerText.trim() : null,
                view: e.querySelector('.movie-rating-container').innerText.trim(),
                sound: d[1].trim(),
                star: d[2].trim(),
                time: d[3].trim() + ':00',
                image: e.querySelector('.movie-card2_img').getAttribute('style').replace('background:url(', '').replace(');', '').trim(),
                dataurl: `${domain}/movie/data/?mid=${mid}`
            })
        });

        res.json({
            title: title,
            totalpage: pagination,
            data: arr
        })
    });
});

module.exports = router;
